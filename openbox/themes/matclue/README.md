### SVG based theme for OpenBox

> For learning purpose.

Created April 2018 by E. Rizqi N. Sayidina <epsi.nurwijayadi@gmail.com>

-- -- --

### Influence

This work had heavy influence from previous work

* Structure: based on example from [openbox.org/wiki/Help:Themes](http://openbox.org/wiki/Help:Themes)

-- -- --

### Material Color

This theme featuring, a few material colors

* Blue

* Green

* Orange

* Pink

* Red

* Yellow

-- -- --

### Public Domain Dedication

Theme released under CC0 1.0 Universal (CC0 1.0) license.
* https://creativecommons.org/publicdomain/zero/1.0/legalcode
