Terminal Ricing
=====================

![terminal ricing][terminal-ricing]

# What's in here

| Category| Package | Path |
| :--- | :--- | :--- |
| terminal | urxvt  | ~/.Xresources, <br/> ~/.Xdefaults |
| terminal | termite  | ~/.config/termite/config |
| shell | oh-my-bash  | ~/.bashrc |
| shell | oh-my-zsh  | ~/.zshrc |
| shell | powerline  | ~/.config/powerline/*, <br/> ~/.bashrc, <br/> ~/.config/fish/config.fish |
| tiling | tmux | ~/.tmux.conf |
| tiling | teamocil | ~/.teamocil/jekyll.yml |
| decoration | compton | ~/.config/compton.conf |
| decoration | gtk-3.0 | ~/.config/gtk-3.0/gtk.css |
| application | vim | ~/.vim/*, <br/> ~/.vimrc |
| application | neofetch | ~/.config/neofetch/config.conf |

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[terminal-ricing]:      https://gitlab.com/epsi-rns/dotfiles/raw/master/terminal/readme/terminal-ricing.png
